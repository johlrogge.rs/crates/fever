// Conversions
use crate::{Celcius, Fahrenheit, Kelvin, Value};

impl<F> From<Fahrenheit<F>> for Celcius<f32>
where
        F: Value + Into<f32>,
{
        fn from(f: Fahrenheit<F>) -> Self {
                let f: f32 = f.0.into();
                let c = (f - 32.0) / 1.8;
                Celcius(c)
        }
}

impl<F> From<Fahrenheit<F>> for Celcius<f64>
where
        F: Value + Into<f64>,
{
        fn from(f: Fahrenheit<F>) -> Self {
                let f: f64 = f.0.into();
                let c = (f - 32.0) / 1.8;
                Celcius(c)
        }
}

macro_rules! from_f_to_c {
        ($fv:ident => $flv:ident => $($cv:ident),+) => {
            $(
                impl From<Fahrenheit<$fv>> for Celcius<$cv>
                {
                        fn from(f: Fahrenheit<$fv>) -> Self {
                            let f:$flv= f.0.into();
                            let c = (f - 32.0) / 1.8;
                            unsafe {
                            	Celcius(c.round().to_int_unchecked())
                            }
                        }
                }
            )*
        };
}

// Setup all safe conversions from fahrenheit to celcius via sufficiently sized float
from_f_to_c!(u8 => f32 => i16, i32, i64, i128, isize);
from_f_to_c!(u16 => f32 => i32, i64, i128);
from_f_to_c!(u32 => f64 => i64, i128);
from_f_to_c!(i8 => f32 => i8, i16, i32, i64, i128, isize);
from_f_to_c!(i16 => f32 => i16, i32, i64, i128);
from_f_to_c!(i32 => f64 => i32, i64, i128);

impl<K> From<Kelvin<K>> for Celcius<f32>
where
        K: Value + Into<f32>,
{
        fn from(f: Kelvin<K>) -> Self {
                let k: f32 = f.0.into();
                let c = k - 273.15;
                Celcius(c)
        }
}
impl<K> From<Kelvin<K>> for Celcius<f64>
where
        K: Value + Into<f64>,
{
        fn from(f: Kelvin<K>) -> Self {
                let k: f64 = f.0.into();
                let c = k - 273.15;
                Celcius(c)
        }
}

macro_rules! from_k_to_c {
        ($fv:ident => $($cv:ident),+) => {
            $(
                impl From<Kelvin<$fv>> for Celcius<$cv>
                {
                        fn from(f: Kelvin<$fv>) -> Self {
                            let k:$cv= f.0.into();
                            let c = k - 273;
                            Celcius(c)
                        }
                }
            )*
        };
}

from_k_to_c!(u8 => i16, i32, i64, i128, isize);
from_k_to_c!(u16 => i32, i64, i128);
from_k_to_c!(u32 => i64, i128);
from_k_to_c!(u64 => i128);
from_k_to_c!(i8 =>  i16, i32, i64, i128, isize);
from_k_to_c!(i16 => i32, i64, i128);
from_k_to_c!(i32 => i64, i128);
from_k_to_c!(i64 => i128);

#[cfg(test)]
#[allow(non_upper_case_globals)]
mod test {

        macro_rules! conversion_test {
            	(@test_method ($from_value:expr), $from_type:ty,  $to_type:ty, $to_value:literal, $to_value_type:ty) => {
                           paste::item! {
                                #[test]
                                fn [<from_ $from_value  _ $from_type:lower _to _ $to_value_type>]() {
                                    assert_eq!($to_type($to_value as $to_value_type), $from_type($from_value).into());
                                }
                           }
            	};

                ($from:ty {$from_value:expr} => $to:ty{$to_value:literal} [$($to_value_type:ty),+]) => {
			$(
                        conversion_test!(@test_method ($from_value), $from, $to, $to_value, $to_value_type);
			)+
	};
                ({$($from_value:expr),+} $from:ty => $to_value:literal $to:ty{ $to_value_type:ty} ) => {
                       $(
                           conversion_test!($from {$from_value} => $to{$to_value} [$to_value_type]);
                       )+
                };
        }

        const u8_min: u8 = u8::MIN;
        const u8_max: u8 = u8::MAX;
        const u16_min: u16 = u16::MIN;
        const u16_max: u16 = u16::MAX;
        const u32_min: u32 = u32::MIN;
        const u32_max: u32 = u32::MAX;
        const i8_min: i8 = i8::MIN;
        const i8_max: i8 = i8::MAX;
        const i16_min: i16 = i16::MIN;
        const i16_max: i16 = i16::MAX;
        const i32_min: i32 = i32::MIN;
        const i32_max: i32 = i32::MAX;
        const i64_min: i64 = i64::MIN;
        const i64_max: i64 = i64::MAX;
        const u64_min: u64 = u64::MIN;
        const u64_max: u64 = u64::MAX;

        mod celcius {

                use super::*;
                use crate::{Celcius, Fahrenheit, Kelvin};

                conversion_test!(Fahrenheit{u8_min}  => Celcius{           -18   } [i16, i32, i64, i128, isize]);
                conversion_test!(Fahrenheit{u8_max}  => Celcius{           124   } [i16, i32, i64, i128, isize]);
                conversion_test!(Fahrenheit{u16_min} => Celcius{           -18   } [i32, i64, i128]);
                conversion_test!(Fahrenheit{u16_max} => Celcius{        36_391   } [i32, i64, i128]);
                conversion_test!(Fahrenheit{u32_min} => Celcius{           -18   } [i64, i128]);
                conversion_test!(Fahrenheit{u32_max} => Celcius{ 2_386_092_924   } [i64, i128]);
                conversion_test!(Fahrenheit{i8_min}  => Celcius{           -89   } [i8, i16, i32, i64, i128, isize]);
                conversion_test!(Fahrenheit{i8_max}  => Celcius{            53   } [i8, i16, i32, i64, i128, isize]);
                conversion_test!(Fahrenheit{i16_min} => Celcius{       -18_222   } [i16, i32, i64, i128]);
                conversion_test!(Fahrenheit{i16_max} => Celcius{        18_186   } [i16, i32, i64, i128]);
                conversion_test!(Fahrenheit{i32_min} => Celcius{-1_193_046_489   } [i32, i64, i128]);
                conversion_test!(Fahrenheit{i32_max} => Celcius{ 1_193_046_453i32} [i32, i64, i128]);
                conversion_test!({0u8, 0u16, 0i8, 0i16, 0f32}                    Fahrenheit =>  -17.77777862548828 Celcius{f32});
                conversion_test!({0u8, 0u16, 0u32, 0i8, 0i16, 0i32, 0f32, 0f64}  Fahrenheit =>  -17.77777777777778 Celcius{f64});

                conversion_test!(Kelvin{u8_min}  => Celcius{                      -273   } [i16, i32, i64, i128, isize]);
                conversion_test!(Kelvin{u8_max}  => Celcius{                       -18   } [i16, i32, i64, i128, isize]);
                conversion_test!(Kelvin{u16_min} => Celcius{                      -273   } [i32, i64, i128]);
                conversion_test!(Kelvin{u16_max} => Celcius{                    65_262   } [i32, i64, i128]);
                conversion_test!(Kelvin{u32_min} => Celcius{                      -273   } [i64, i128]);
                conversion_test!(Kelvin{u32_max} => Celcius{             4_294_967_022   } [i64, i128]);
                conversion_test!(Kelvin{u64_min} => Celcius{                      -273   } [i128]);
                conversion_test!(Kelvin{u64_max} => Celcius{18_446_744_073_709_551_342   } [i128]);
                conversion_test!(Kelvin{i8_min}  => Celcius{                      -401   } [i16, i32, i64, i128, isize]);
                conversion_test!(Kelvin{i8_max}  => Celcius{                      -146   } [i16, i32, i64, i128, isize]);
                conversion_test!(Kelvin{i16_min} => Celcius{                   -33_041   } [i32, i64, i128]);
                conversion_test!(Kelvin{i16_max} => Celcius{                    32_494   } [i32, i64, i128]);
                conversion_test!(Kelvin{i32_min} => Celcius{            -2_147_483_921   } [i64, i128]);
                conversion_test!(Kelvin{i32_max} => Celcius{             2_147_483_374i32} [i64, i128]);
                conversion_test!(Kelvin{i64_min} => Celcius{-9_223_372_036_854_776_081   } [i128]);
                conversion_test!(Kelvin{i64_max} => Celcius{ 9_223_372_036_854_775_534   } [i128]);
                conversion_test!({0u8, 0u16, 0i8, 0i16, 0f32}                    Kelvin => -273.15 Celcius{f32});
                conversion_test!({0u8, 0u16, 0u32, 0i8, 0i16, 0i32, 0f32, 0f64}  Kelvin => -273.15 Celcius{f64});
        }
}
