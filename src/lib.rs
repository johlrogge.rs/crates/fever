use serde::{Deserialize, Serialize};

use std::fmt::Debug;
use std::fmt::Display;
use std::ops::Div;
use std::ops::Sub;

pub mod convert;

/// A marker trait for values that can represent temperatures
pub trait Value: Display + Clone + Debug + PartialOrd + Sub + Div {}
impl<T> Value for T where T: Display + Clone + Debug + PartialEq + PartialOrd + Sub + Div {}

macro_rules! unit {
    ($struct:ident, $default_type:ident,  $nominator:literal) => {
        #[derive(Clone, Debug, PartialEq, PartialOrd)]
        pub struct $struct<T: Value = $default_type>(T);

        impl<T: Value> Display for $struct<T> {
            fn fmt(
                &self,
                fmt: &mut std::fmt::Formatter<'_>,
            ) -> std::result::Result<(), std::fmt::Error> {
                write!(fmt, "{}°{}", self.0, $nominator)
            }
        }

        impl<T> Ord for $struct<T>
        where
            T: Ord + Value,
        {
            fn cmp(&self, other: &Self) -> std::cmp::Ordering {
                self.0.cmp(&other.0)
            }
        }

        impl<T> Eq for $struct<T> where T: Eq + Value {}

        impl<T> From<T> for $struct<T>
        where
            T: Value,
        {
            fn from(value: T) -> Self {
                $struct(value)
            }
        }
    };
}

unit!(Celcius, isize, 'C');
unit!(Fahrenheit, isize, 'F');
unit!(Kelvin, usize, 'K');

macro_rules! test_unit {
    ($test_module:ident, $unit:ident, $i:literal, $f:literal, $unit_literal:literal) => {
        #[cfg(test)]
        mod $test_module {
            use super::$unit;
            #[test]
            fn can_be_represented_by_all_primitives() {
                let _usize = $unit($i as usize);
                let _isize = $unit($i as isize);
                let _i8 = $unit($i as i8);
                let _i16 = $unit($i as i16);
                let _i32 = $unit($i as i32);
                let _i64 = $unit($i as i64);
                let _i128 = $unit($i as i128);
                let _u8 = $unit($i as u8);
                let _u16 = $unit($i as u16);
                let _u32 = $unit($i as u32);
                let _u64 = $unit($i as u64);
                let _u128 = $unit($i as u128);
                let _f32 = $unit($i as f32);
                let _f64 = $unit($i as f64);
            }

            #[test]
            fn can_be_represented_by_references_to_all_primitives() {
                let _usize = $unit(&($i as usize));
                let _isize = $unit(&($i as isize));
                let _i8 = $unit(&($i as i8));
                let _i16 = $unit(&($i as i16));
                let _i32 = $unit(&($i as i32));
                let _i64 = $unit(&($i as i64));
                let _i128 = $unit(&($i as i128));
                let _u8 = $unit(&($i as u8));
                let _u16 = $unit(&($i as u16));
                let _u32 = $unit(&($i as u32));
                let _u64 = $unit(&($i as u64));
                let _u128 = $unit(&($i as u128));
                let _f32 = $unit(&($i as f32));
                let _f64 = $unit(&($i as f64));
            }

            #[test]
            fn display_integers() {
                assert_eq!(
                    format!("{}°{}", $i, $unit_literal),
                    format!("{}", $unit($i))
                );
            }

            #[test]
            fn display_floats() {
                assert_eq!(
                    format!("{}°{}", $f, $unit_literal),
                    format!("{}", $unit($f))
                );
            }

            #[test]
            fn equality() {
                assert_eq!($unit($i), $unit($i));
                assert_ne!($unit($i), $unit($i + 1));
            }

            #[test]
            fn order() {
                assert!($unit($i) < $unit($i + 1));
            }

            #[test]
            fn convert_from_unsigned() {
                assert_eq!($unit::<usize>($i as usize), ($i as usize).into());
                assert_eq!($unit::<u8>($i as u8), ($i as u8).into());
                assert_eq!($unit::<u16>($i as u16), ($i as u16).into());
                assert_eq!($unit::<u32>($i as u32), ($i as u32).into());
                assert_eq!($unit::<u64>($i as u64), ($i as u64).into());
                assert_eq!($unit::<u128>($i as u128), ($i as u128).into());
            }

            #[test]
            fn convert_from_signed() {
                assert_eq!($unit::<isize>($i as isize), ($i as isize).into());
                assert_eq!($unit::<i8>($i as i8), ($i as i8).into());
                assert_eq!($unit::<i16>($i as i16), ($i as i16).into());
                assert_eq!($unit::<i32>($i as i32), ($i as i32).into());
                assert_eq!($unit::<i64>($i as i64), ($i as i64).into());
                assert_eq!($unit::<i128>($i as i128), ($i as i128).into());
            }
        }
    };
}

test_unit!(test_celcius, Celcius, 37, 37.5, "C");
test_unit!(test_fahrenheit, Fahrenheit, 100, 100.5, "F");
test_unit!(test_kelvin, Kelvin, 120, 310.15, "K");

// serialization
macro_rules! ser_de {
    ($for_type:ident) => {
        impl<T> Serialize for $for_type<T>
        where
            T: Value + Serialize,
        {
            fn serialize<S>(
                &self,
                serializer: S,
            ) -> std::result::Result<<S as serde::Serializer>::Ok, <S as serde::Serializer>::Error>
            where
                S: serde::Serializer,
            {
                self.0.serialize(serializer)
            }
        }

        impl<'de, T> Deserialize<'de> for $for_type<T>
        where
            T: Value + Deserialize<'de>,
        {
            fn deserialize<D>(
                deserializer: D,
            ) -> std::result::Result<Self, <D as serde::Deserializer<'de>>::Error>
            where
                D: serde::Deserializer<'de>,
            {
                T::deserialize(deserializer).map($for_type)
            }
        }
    };
}

ser_de!(Celcius);
ser_de!(Fahrenheit);
ser_de!(Kelvin);
#[cfg(test)]
mod test_serde {
    use super::{Celcius, Fahrenheit, Kelvin};
    use serde_test::{assert_tokens, Token};
    #[test]
    fn serialize_deserialize_celcius() {
        assert_tokens(&Celcius(37_i32), &[Token::I32(37)]);
    }

    #[test]
    fn serialize_deserialize_fahrenheit() {
        assert_tokens(&Fahrenheit(100_i32), &[Token::I32(100)]);
    }

    #[test]
    fn serialize_deserialize_kelvint() {
        assert_tokens(&Kelvin(310.15_f32), &[Token::F32(310.15)]);
    }
}

pub mod serialize {
    pub mod centi_celcius {
        use crate::Celcius;
        use serde::{Deserialize, Deserializer, Serializer};
        pub fn serialize<S>(val: &Celcius<f32>, s: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            let Celcius(v) = val;
            s.serialize_i32((v * 100.0) as i32)
        }

        pub fn deserialize<'de, D>(deserializer: D) -> Result<Celcius<f32>, D::Error>
        where
            D: Deserializer<'de>,
        {
            let val: f32 = i32::deserialize(deserializer)? as f32;
            Ok(Celcius(val / 100.0))
        }
    }
    pub mod option_centi_celcius {
        use crate::Celcius;
        use serde::{Deserialize, Deserializer, Serializer};

        pub fn serialize<S>(val: &Option<Celcius<f32>>, s: S) -> Result<S::Ok, S::Error>
        where
            S: Serializer,
        {
            if let Some(Celcius(v)) = val {
                s.serialize_i32((v * 100.0) as i32)
            } else {
                unreachable!("should not get here!")
            }
        }

        pub fn deserialize<'de, D>(deserializer: D) -> Result<Option<Celcius<f32>>, D::Error>
        where
            D: Deserializer<'de>,
        {
            let maybe_val = Some(i32::deserialize(deserializer)? as f32);
            Ok(maybe_val.map(|val| Celcius(val / 100.0)))
        }
    }
}

#[cfg(test)]
mod test_custom_deserialization {
    use super::{serialize, Celcius};
    use serde::{Deserialize, Serialize};
    use serde_test::{assert_tokens, Token};

    #[derive(PartialEq, Debug, Serialize, Deserialize)]
    struct CentiCelciusDeserialization(#[serde(with = "serialize::centi_celcius")] Celcius<f32>);

    #[derive(PartialEq, Debug, Serialize, Deserialize)]
    struct OptionCentiCelciusDeserialization(
        #[serde(
            with = "serialize::option_centi_celcius",
            skip_serializing_if = "Option::is_none"
        )]
        Option<Celcius<f32>>,
    );

    #[test]
    fn serialize_deserialize_centi_celcius() {
        assert_tokens(
            &CentiCelciusDeserialization(Celcius(37.62_f32)),
            &[
                Token::NewtypeStruct {
                    name: "CentiCelciusDeserialization",
                },
                Token::I32(3762),
            ],
        );
    }

    #[test]
    fn serialize_deserialize_option_centi_celcius() {
        assert_tokens(
            &OptionCentiCelciusDeserialization(Some(Celcius(37.62_f32))),
            &[
                Token::NewtypeStruct {
                    name: "OptionCentiCelciusDeserialization",
                },
                Token::I32(3762),
            ],
        );
    }
}
